## 
## Filename     : Makefile
## Description  : Makefile for the python weather underground library
## Author       : Christophe Burki
## Version      : 1.0.0
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License version 3 as
## published by the Free Software Foundation.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file LICENSE.  If not, write to the
## Free Software Foundation, Inc., 51 Franklin Street, Fifth
## ;; Floor, Boston, MA 02110-1301, USA.
## 
######################################################################

#
# --------------------------------------------------------------------
#

PYTHON := python3


#
# --------------------------------------------------------------------
#

.PHONY : build clean publish

build :
	$(PYTHON) setup.py sdist

clean : 
	rm -f *~
	rm -f pyunderground/*.pyc

distclean : clean
	rm -f dist/*
	rm -f *.log

check :
	flake8 --ignore=W291,E302,E501

test :
	$(PYTHON) pywunderground_test.py

get :
	pip install arrow
	pip install urllib3

install : build get
	pip install --upgrade dist/pywunderground-$(shell cat VERSION).tar.gz
