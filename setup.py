"""
Created on Apr 14, 2016

@author: Christophe Burki
@author_email: christophe.burki@gmail.com
"""

# ------------------------------------------------------------------------------

from distutils.core import setup

# ------------------------------------------------------------------------------

def version():
    """
    Read the version from VERSION file
    """

    f = open('VERSION', 'r')
    v = f.readline().strip()
    f.close()

    return v

# ------------------------------------------------------------------------------

setup(name='pywunderground',
      version=version(),
      description='Python wrapper arround the Weather Undergound API',
      author='Christophe Burki',
      author_email='christophe.burki@gmail.com',
      url='http://gitlab.burkionline.net:8443/cburki/pywunderground',
      packages=['pywunderground'],
      data_files=[('./', ['VERSION'])])

# ------------------------------------------------------------------------------
