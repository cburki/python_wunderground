#
# Filename     : astronomy.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from json import dumps, loads

import arrow

# ------------------------------------------------------------------------------

class Astronomy:
    """
    Encapsulate raw astronomy data.
    """

    # --------------------------------------------------------------------------

    def __init__(self,
                 hemisphere,
                 sunPhase,
                 moonPhase):
        """
        Constructor

        :param hemmisphere: The hemisphere relative to the location
        :type hemisphere: string
        :param sunPhase: The sun phase data
        :type sunPhase: SunPhase object
        :param moonPhase: The moon phase data
        :type moonPhase: MoonPhase object
        """

        self._hemisphere = hemisphere
        self._sunPhase = sunPhase
        self._moonPhase = moonPhase

    # --------------------------------------------------------------------------

    def getHemisphere(self):
        """
        Return the hemisphere relative to the location.
        """

        return self._hemisphere

    # --------------------------------------------------------------------------

    def getSunPhase(self):
        """
        Return the sun phase object.
        """

        return self._sunPhase

    # --------------------------------------------------------------------------

    def getMoonPhase(self):
        """
        Return the moon phase object.
        """

        return self._moonPhase

    # --------------------------------------------------------------------------

    def toJSON(self):
        """
        Return the JSON formatted representation of the object.
        """

        return dumps({'hemisphere': self._hemisphere,
                      'sunPhase': loads(self._sunPhase.toJSON()),
                      'moonPhase': loads(self._moonPhase.toJSON())})

    # --------------------------------------------------------------------------

    def __str__(self):
        """
        Return the string representation of the object.
        """

        return '{} : {} {} | {} {}'.format(self._hemisphere,
                                           arrow.get(self._sunPhase.getSunriseTime()),
                                           arrow.get(self._sunPhase.getSunsetTime()),
                                           arrow.get(self._moonPhase.getMoonriseTime()),
                                           arrow.get(self._moonPhase.getMoonsetTime()))

# ------------------------------------------------------------------------------
