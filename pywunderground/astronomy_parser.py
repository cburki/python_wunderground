#
# Filename     : astronomy_parser.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from pywunderground.parser import Parser
from pywunderground.astronomy import Astronomy
from pywunderground.sun_phase import SunPhase
from pywunderground.moon_phase import MoonPhase
from pywunderground.exceptions import ParserError

import arrow

# ------------------------------------------------------------------------------

class AstronomyParser(Parser):
    """
    Concrete parser implementation building an astronomy object.
    """

    # --------------------------------------------------------------------------

    def __init__(self):
        """
        Constructor
        """

        pass

    # --------------------------------------------------------------------------

    @staticmethod
    def parse(data):
        """
        Return an moon phase object parsed from the given data.

        :param data: The JSON data to parse
        :type data: JSON
        """

        hemisphere = 'Unknown'
        moonPhase = None
        sunPhase = None

        # Check that required data are presents.
        required = ['astronomy']
        for key in required:
            if key not in data:

                raise ParserError('Missing data', key)

        try:
            astronomyData = data['astronomy']

            # Get the hemisphere.
            hemisphere = astronomyData['hemisphere']

            sunrise = arrow.now().replace(hour=int(astronomyData['sunrise']['hour']), minute=int(astronomyData['sunrise']['minute'])).timestamp
            sunset = arrow.now().replace(hour=int(astronomyData['sunset']['hour']), minute=int(astronomyData['sunset']['minute'])).timestamp
            moonrise = arrow.now().replace(hour=int(astronomyData['moonrise']['hour']), minute=int(astronomyData['moonrise']['minute'])).timestamp
            moonset = arrow.now().replace(hour=int(astronomyData['moonset']['hour']), minute=int(astronomyData['moonset']['minute'])).timestamp

            # Build sun phase object.
            sunPhase = SunPhase(sunriseTime=sunrise,
                                sunsetTime=sunset)

            # Build moon phase object.
            moonPhase = MoonPhase(percentIlluminated=int(astronomyData['percentIlluminated']),
                                  age=int(astronomyData['ageOfMoon']),
                                  phase=astronomyData['phaseofMoon'],
                                  moonriseTime=moonrise,
                                  moonsetTime=moonset)

        except KeyError as e:
            raise ParserError('Missing key while parsing astronomy data', '', e)

        # Build astronomy object.
        return Astronomy(hemisphere, sunPhase, moonPhase)

# ------------------------------------------------------------------------------
