#
# Filename     : observation_parser.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from pywunderground.parser import Parser
from pywunderground.weather import Weather
from pywunderground.location import Location
from pywunderground.observation import Observation
from pywunderground.time_utils import isDay
from pywunderground.exceptions import ParserError

import arrow

# ------------------------------------------------------------------------------

class ObservationParser(Parser):
    """
    Concrete parser implementation building an observation object.
    """

    # --------------------------------------------------------------------------

    def __init__(self):
        """
        Constructor
        """

        pass

    # --------------------------------------------------------------------------

    @staticmethod
    def parse(data):
        """
        Return an observation object parsed from the given data.

        :param data: The JSON data to parse
        :type data: JSON
        """

        weather = None
        location = None

        # Check that required data are presents.
        required = ['observation', 'daily_forecast', 'astronomy']
        for key in required:
            if key not in data:

                raise ParserError('Missing data', key)

        # Build weather object.
        try:
            observationData = data['observation']
            forecastData = data['daily_forecast']['simpleforecast']['forecastday'][0]
            astronomyData = data['astronomy']

            sunrise = arrow.now().replace(hour=int(astronomyData['sunrise']['hour']), minute=int(astronomyData['sunrise']['minute'])).timestamp
            sunset = arrow.now().replace(hour=int(astronomyData['sunset']['hour']), minute=int(astronomyData['sunset']['minute'])).timestamp

            weather = Weather(referenceTime=int(observationData['observation_epoch']),
                              temperature={'temp': float(observationData['temp_c']),
                                           'min': float(forecastData['low']['celsius']),
                                           'max': float(forecastData['high']['celsius'])},
                              humidity=float(observationData['relative_humidity'][:-1]),
                              pressure=float(observationData['pressure_mb']),
                              wind={'speed': float(observationData['wind_kph']),
                                    'deg': int(observationData['wind_degrees']),
                                    'dir': observationData['wind_dir']},
                              rain={'all': float(forecastData['qpf_allday']['mm'])},
                              snow={'all': float(forecastData['snow_allday']['cm'])},
                              status=observationData['weather'],
                              icon=observationData['icon'] if isDay(sunrise, sunset) else 'nt_{}'.format(observationData['icon']),
                              clouds={'all': None},
                              dewpoint=observationData['dewpoint_c'],
                              visibilityDistance=float(observationData['visibility_km']) if observationData['visibility_km'].replace('.', '').isdigit() else -9999.0,
                              heatIndex=observationData['heat_index_c'] if observationData['heat_index_c'].replace('.', '').isdigit() else -9999.0,
                              chancePrecipitation=None)

        except KeyError as e:
            raise ParserError('Missing key while parsing weather data', '', e)

        # Build location object.
        try:
            locationData = data['observation']['display_location']
            location = Location(name=locationData['city'],
                                country=locationData['state_name'],
                                countryCode=locationData['country_iso3166'],
                                latitude=float(locationData['latitude']),
                                longitude=float(locationData['longitude']))

        except KeyError as e:
            raise ParserError('Missing key while parsing location data', '', e)

        return Observation(location, weather)

# ------------------------------------------------------------------------------
