#
# Filename     : forecast.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from json import dumps, loads

# ------------------------------------------------------------------------------

class Forecast:
    """
    Encapsulate weather forecast data for a certain location and relative to
    a certain time interval (forecast for every three hours or every days).
    """

    # --------------------------------------------------------------------------

    def __init__(self,
                 interval,
                 location,
                 weathers):
        """
        Constructor

        :param interval: The time granularity of the forecast. May be 'h' for hourly forecast or 'd' for daily forecast
        :type interval: string
        :param location: The location object relative to the forecast
        :type location: Location object
        :param weathers: The list of weather objects composing the forecast
        :type weathers: list
        """

        self._interval = interval
        self._location = location
        self._weathers = weathers

    # --------------------------------------------------------------------------

    def getInterval(self):
        """
        Return the time granularity of the forecast.
        """

        return self._interval

    # --------------------------------------------------------------------------

    def getLocation(self):
        """
        Return the location object relative to the forecast.
        """

        return self._location

    # --------------------------------------------------------------------------

    def getWeathers(self):
        """
        Return the weather object composing the forecast.
        """

        return self._weathers

    # --------------------------------------------------------------------------

    def getWeather(self, index):
        """
        Return the weather at the given index.
        """

        try:
            return self._weathers[index]
        except IndexError:
            return None

    # --------------------------------------------------------------------------

    def countWeathers(self):
        """
        Return the number of items that compose the forecast.
        """

        return len(self._weathers)

    # --------------------------------------------------------------------------

    def toJSON(self):
        """
        Return the JSON formatted representation of the object.
        """

        return dumps({'interval': self._interval,
                      'location': loads(self._location.toJSON()),
                      'weathers': loads('[{}]'.format(','.join([w.toJSON() for w in self._weathers])))})

    # --------------------------------------------------------------------------

    def __str__(self):
        """
        Return the string representation of the object.
        """

        return '{}, {} : {} forecasts'.format(self._location.getName(),
                                              self._location.getCountry(),
                                              len(self._weathers))

# ------------------------------------------------------------------------------
