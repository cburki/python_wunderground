#
# Filename     : time_utils.py
# Description  : Utility functions to work with times.
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

import arrow

# ------------------------------------------------------------------------------

def isDay(sunrise, sunset):
    """
    Return whether the current time is during the day or not. Current time is
    the day it is between sunrise and sunset.

    :param sunrise: The sunrise timestamp
    :type sunrise: int
    :param sunset: The sunset timestamp
    :type sunset: int
    """

    now = arrow.now().timestamp
    return (now >= sunrise and now <= sunset)

# ------------------------------------------------------------------------------
