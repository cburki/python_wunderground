#
# Filename     : parser.py
# Description  : Abstract class for a JSON object parser.
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from abc import ABCMeta, abstractmethod

# ------------------------------------------------------------------------------

class Parser:
    """
    Abstract class representing a JSON object parser.
    """

    __metaclass__ = ABCMeta

    # --------------------------------------------------------------------------

    @abstractmethod
    def parse(self, data):
        """
        Return a proper object parsed fron the given JSON data.

        :param data: The JSON data to parse
        :type data: JSON
        """

        raise NotImplementedError

# ------------------------------------------------------------------------------
