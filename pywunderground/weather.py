#
# Filename     : weather.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from json import dumps

import arrow

# ------------------------------------------------------------------------------

class Weather:
    """
    Encapsulate raw weather data.
    """

    # --------------------------------------------------------------------------

    def __init__(self,
                 referenceTime,
                 temperature,
                 humidity,
                 pressure,
                 wind,
                 rain,
                 snow,
                 status,
                 icon,
                 clouds,
                 dewpoint,
                 visibilityDistance,
                 heatIndex,
                 chancePrecipitation):
        """
        Constructor

        :param referenceTime: Timestamp of weather measurement
        :type referenceTime: int
        :param temperature: Temperature information
        :type temperature: float
        :param humidity: Atmospheric humidity percentage
        :type humidity: float
        :param pressure: Atmospheric pressure information
        :type pressure: float
        :param wind: Wind information
        :type wind: dict
        :param rain: Precipitation information
        :type rain: dict
        :param snow: Snow information
        :type snow: dict
        :param status: Short weather status
        :type status: string
        :param icon: Weather condition icon
        :type icon: string
        :param clouds: Cloud coverage percentage
        :type clouds:
        :param dewpoint: Dewpoint
        :type dewpoint:
        :param visibilityDistance: Visibility distance
        :type visibilityDistance:
        :param heatIndex: Heat index
        :type heatIndex:
        :param chancePrecipitation: Chance (probability) of precipitation
        :type chancePrecipitation: int
        """

        self._referenceTime = referenceTime
        self._temperature = temperature       # {'temp' : None, 'min' : None, 'max' : None}
        self._humidity = humidity
        self._pressure = pressure
        self._wind = wind                     # {'speed' : None, 'dir' : None, 'deg' : None}
        self._rain = rain                     # {'all' : None}  today rain volume [mm]
        self._snow = snow                     # {'all' : None}  today snow volume [cm]
        self._status = status
        self._icon = icon
        self._clouds = clouds                 # {'all' : None}
        self._dewpoint = dewpoint
        self._visibilityDistance = visibilityDistance
        self._heatIndex = heatIndex
        self._chancePrecipitation = chancePrecipitation

    # --------------------------------------------------------------------------

    def getReferenceTime(self):
        """
        Return the time when the weather was measured.
        """

        return self._referenceTime

    # --------------------------------------------------------------------------

    def getTemperature(self):
        """
        Return the temperatures.
        """

        return self._temperature

    # --------------------------------------------------------------------------

    def getHumidity(self):
        """
        Return the humidity percentage.
        """

        return self._humidity

    # --------------------------------------------------------------------------

    def getPressure(self):
        """
        Return the atmospheric pressure.
        """

        return self._pressure

    # --------------------------------------------------------------------------

    def getWind(self):
        """
        Return the wind information.
        """

        return self._wind

    # --------------------------------------------------------------------------

    def getRain(self):
        """
        Return the precipitation information.
        """

        return self._rain

    # --------------------------------------------------------------------------

    def getSnow(self):
        """
        Return the snow information.
        """

        return self._snow

    # --------------------------------------------------------------------------

    def getStatus(self):
        """
        Return the weather status.
        """

        return self._status

    # --------------------------------------------------------------------------

    def getIcon(self):
        """
        Return the icon name.
        """

        return self._icon

    # --------------------------------------------------------------------------

    def getClouds(self):
        """
        Return the clouds coverage percentage.
        """

        return self._clouds

    # --------------------------------------------------------------------------

    def getDewpoint(self):
        """
        Return the dewpoint.
        """

        return self._dewpoint

    # --------------------------------------------------------------------------

    def getVisibilityDistance(self):
        """
        Return the visibility distance.
        """

        return self._visibilityDistance

    # --------------------------------------------------------------------------

    def getHeatIndex(self):
        """
        Return the heat index.
        """

        return self._heatIndex

    # --------------------------------------------------------------------------

    def getChancePrecipitation(self):
        """
        Return the chance of precipitation.
        """

        return self._chancePrecipitation

    # --------------------------------------------------------------------------

    def toJSON(self):
        """
        Return the JSON formatted representation of the object.
        """

        return dumps({'referenceTime': self._referenceTime,
                      'temperature': self._temperature,
                      'humidity': self._humidity,
                      'pressure': self._pressure,
                      'wind': self._wind,
                      'rain': self._rain,
                      'snow': self._snow,
                      'status': self._status,
                      'icon': self._icon,
                      'clouds': self._clouds,
                      'dewpoint': self._dewpoint,
                      'visibilityDistance': self._visibilityDistance,
                      'heatIndex': self._heatIndex})

    # --------------------------------------------------------------------------

    def __str__(self):
        """
        Return the string representation of the object.
        """

        return '{} : {} ({})'.format(arrow.get(self._referenceTime),
                                     self._status,
                                     self._icon)

# ------------------------------------------------------------------------------
