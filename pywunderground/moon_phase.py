#
# Filename     : moon_phase.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from json import dumps

import arrow

# ------------------------------------------------------------------------------

class MoonPhase:
    """
    Encapsulate raw mon phase data.
    """

    # --------------------------------------------------------------------------

    def __init__(self,
                 percentIlluminated,
                 age,
                 phase,
                 moonriseTime,
                 moonsetTime):
        """
        Constructor

        :param percentIlluminated: The percentage of illumination
        :type percentIlluminated: int
        :param age: The age of the moon
        :type age: int
        :param phase: The phase of the moon
        :type phase: string
        :param moonriseTime: Timestamp of moonrise
        :type moonriseTime: int
        :param moonsetTime: Timestamp of the moonset
        :type moonsetTime: int
        """

        self._percentIlluminated = percentIlluminated
        self._age = age
        self._phase = phase
        self._moonriseTime = moonriseTime
        self._moonsetTime = moonsetTime

    # --------------------------------------------------------------------------

    def getPercentIlluminated(self):
        """
        Return the percentage of illumination.
        """

        return self._percentIlluminated

    # --------------------------------------------------------------------------

    def getAge(self):
        """
        Return the age of the moon.
        """

        return self._age

    # --------------------------------------------------------------------------

    def getPhase(self):
        """
        Return the phase of the moon.
        """

        return self._phase

    # --------------------------------------------------------------------------

    def getMoonriseTime(self):
        """
        Return the time of the moonrise.
        """

        return self._moonriseTime

    # --------------------------------------------------------------------------

    def getMoonsetTime(self):
        """
        Return the time of the moonset.
        """

        return self._moonsetTime

    # --------------------------------------------------------------------------

    def toJSON(self):
        """
        Return the JSON formatted representation of the object.
        """

        return dumps({'percentIlluminated': self._percentIlluminated,
                      'age': self._age,
                      'phase': self._phase,
                      'hemisphere': self._hemisphere,
                      'moonriseTime': self._moonriseTime,
                      'moonsetTime': self._moonsetTime})

    # --------------------------------------------------------------------------

    def __str__(self):
        """
        Return the string representation of the object.
        """

        return '{} ({}) : {} {}'.format(self._phase,
                                        self._age,
                                        arrow.get(self._moonriseTime),
                                        arrow.get(self._moonsetTime))

# ------------------------------------------------------------------------------
