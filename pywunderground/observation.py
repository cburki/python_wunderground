#
# Filename     : observation.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from json import dumps, loads

import arrow

# ------------------------------------------------------------------------------

class Observation:
    """
    Represent the weather which is currently observed ina certain location.
    """

    # --------------------------------------------------------------------------

    def __init__(self,
                 location,
                 weather):
        """
        Constructor

        :param location: The location relative to this observation
        :type location: Location object
        :param weather: The weather relative to this observation
        :type weather: Weather object
        """

        self._location = location
        self._weather = weather

    # --------------------------------------------------------------------------

    def getLocation(self):
        """
        Return teh location object for this observation.
        """

        return self._location

    # --------------------------------------------------------------------------

    def getWeather(self):
        """
        Return the weather object for this observation.
        """

        return self._weather

    # --------------------------------------------------------------------------

    def toJSON(self):
        """
        Return the JSON formatted representation of the object.
        """

        return dumps({'location': loads(self._location.toJSON()),
                      'weather': loads(self._weather.toJSON())})

    # --------------------------------------------------------------------------

    def __str__(self):
        """
        Return the string representation of the object.
        """

        return '{} : {}, {} : {} ({})'.format(arrow.get(self._weather.getReferenceTime()),
                                              self._location.getName(),
                                              self._location.getCountry(),
                                              self._weather.getStatus(),
                                              self._weather.getIcon())

# ------------------------------------------------------------------------------
