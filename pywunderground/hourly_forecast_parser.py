#
# Filename     : forecast_parser.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from pywunderground.parser import Parser
from pywunderground.weather import Weather
from pywunderground.location import Location
from pywunderground.forecast import Forecast
from pywunderground.exceptions import ParserError

# ------------------------------------------------------------------------------

class HourlyForecastParser(Parser):
    """
    Concrete parser implementation building a hourly forecast object.
    """

    # --------------------------------------------------------------------------

    def __init__(self):
        """
        Constructor
        """

        pass

    # --------------------------------------------------------------------------

    @staticmethod
    def parse(data):
        """
        Return a hourly forecast object parsed from the given data.

        :param data: The JSON data to parse
        :type data: JSON
        """

        interval = 'h'
        location = None
        weathers = []

        # Check that the required data are presents.
        required = ['observation', 'hourly_forecast']
        for key in required:
            if key not in data:

                raise ParserError('Missing data', key)

        # Build location object.
        try:
            locationData = data['observation']['display_location']
            location = Location(name=locationData['city'],
                                country=locationData['state_name'],
                                countryCode=locationData['country_iso3166'],
                                latitude=locationData['latitude'],
                                longitude=locationData['longitude'])

        except KeyError as e:
            raise ParserError('Missing key while parsing location data', '', e)

        # Build the forecast objects.
        try:
            for forecastData in data['hourly_forecast']:

                weather = Weather(referenceTime=int(forecastData['FCTTIME']['epoch']),
                                  temperature={'temp': float(forecastData['temp']['metric']),
                                               'min': None,
                                               'max': None},
                                  humidity=float(forecastData['humidity']),
                                  pressure=float(forecastData['mslp']['metric']),
                                  wind={'speed': float(forecastData['wspd']['metric']),
                                        'deg': int(forecastData['wdir']['degrees']),
                                        'dir': forecastData['wdir']['dir']},
                                  rain={'all': float(forecastData['qpf']['metric'])},
                                  snow={'all': float(forecastData['snow']['metric'])},
                                  status=forecastData['condition'],
                                  icon=forecastData['icon'],
                                  clouds={'all': None},
                                  dewpoint=forecastData['dewpoint']['metric'],
                                  visibilityDistance=None,
                                  heatIndex=forecastData['heatindex']['metric'],
                                  chancePrecipitation=forecastData['pop'])

                weathers.append(weather)

        except KeyError as e:
            raise ParserError('Missing key while parsing forecast data', '', e)

        return Forecast(interval, location, weathers)

# ------------------------------------------------------------------------------
