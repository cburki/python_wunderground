#
# Filename     : location.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from json import dumps

# ------------------------------------------------------------------------------

class Location:
    """
    Represent a location in the word.
    """

    # --------------------------------------------------------------------------

    def __init__(self,
                 name,
                 country,
                 countryCode,
                 latitude,
                 longitude):
        """
        Constructor

        :param name: The location's name
        :type name: string
        :param country: The location's country
        :type country: string
        :param countryCode: The ISO 33166 country code
        :type countryCode: string
        :param latitude: The location's latitude
        :type latitude: float
        :param longitude: The location's longitude
        :type longitude: float
        """

        self._name = name
        self._country = country
        self._countryCode = countryCode
        self._latitude = latitude
        self._longitude = longitude

    # --------------------------------------------------------------------------

    def getName(self):
        """
        Return the location id.
        """

        return self._name

    # --------------------------------------------------------------------------

    def getCountry(self):
        """
        Return the country of the location.
        """

        return self._country

    # --------------------------------------------------------------------------

    def getCountryCode(self):
        """
        Return the country code of the location.
        """

        return self._countryCode

    # --------------------------------------------------------------------------

    def getLatitude(self):
        """
        Return the latitude of the location.
        """

        return self._latitude

    # --------------------------------------------------------------------------

    def getLongitude(self):
        """
        Return the longitude of the location.
        """

        return self._longitude

    # --------------------------------------------------------------------------

    def toJSON(self):
        """
        Return the JSON formatted representation of the object.
        """

        return dumps({'id': self._id,
                      'name': self._name,
                      'country': self._country,
                      'latitude': self._latitude,
                      'longitude': self._longitude})

    # --------------------------------------------------------------------------

    def __str__(self):
        """
        Return the string representation of the object.
        """

        return '{} ({}) | {}, {}'.format(self._name, self._country, self.latitude, self.longitude)

# ------------------------------------------------------------------------------
