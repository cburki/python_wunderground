#
# Filename     : forecast_parser.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from pywunderground.parser import Parser
from pywunderground.weather import Weather
from pywunderground.location import Location
from pywunderground.forecast import Forecast
from pywunderground.exceptions import ParserError

# ------------------------------------------------------------------------------

class DailyForecastParser(Parser):
    """
    Concrete parser implementation building a daily forecast object.
    """

    # --------------------------------------------------------------------------

    def __init__(self):
        """
        """

        pass

    # --------------------------------------------------------------------------

    @staticmethod
    def parse(data):
        """
        Return a daily forecast object parsed from the given data.

        :param data: The JSON data to parse
        :type data: JSON
        """

        interval = 'd'
        location = None
        weathers = []

        # Check that the required data are presents.
        required = ['observation', 'daily_forecast']
        for key in required:
            if key not in data:
                raise ParserError('Missing data', key)

        # Build location object.
        try:
            locationData = data['observation']['display_location']
            location = Location(name=locationData['city'],
                                country=locationData['state_name'],
                                countryCode=locationData['country_iso3166'],
                                latitude=float(locationData['latitude']),
                                longitude=float(locationData['longitude']))

        except KeyError as e:
            raise ParserError('Missing key while parsing location data', '', e)

        # Build the forecast objects.
        try:
            for forecastData in data['daily_forecast']['simpleforecast']['forecastday']:

                tempMin = float(forecastData['low']['celsius'])
                tempMax = float(forecastData['high']['celsius'])

                weather = Weather(referenceTime=int(forecastData['date']['epoch']),
                                  temperature={'temp': (tempMin + tempMax) / 2,
                                               'min': tempMin,
                                               'max': tempMax},
                                  humidity=float(forecastData['avehumidity']),
                                  pressure=None,
                                  wind={'speed': float(forecastData['avewind']['kph']),
                                        'deg': int(forecastData['avewind']['degrees']),
                                        'dir': forecastData['avewind']['dir']},
                                  rain={'all': float(forecastData['qpf_allday']['mm'])},
                                  snow={'all': float(forecastData['snow_allday']['cm'])},
                                  status=forecastData['conditions'],
                                  icon=forecastData['icon'],
                                  clouds={'all': None},
                                  dewpoint=None,
                                  visibilityDistance=None,
                                  heatIndex=None,
                                  chancePrecipitation=forecastData['pop'])

                weathers.append(weather)

        except KeyError as e:
            raise ParserError('Missing key while parsing forecast data', '', e)

        return Forecast(interval, location, weathers)

# ------------------------------------------------------------------------------
