#
# Filename     : __init__.py
# Description  : 
# Author       : Christophe Burki
# Version      : 1.0.0
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from pywunderground.weather_underground import WeatherUnderground

# ------------------------------------------------------------------------------

def WU(apiKey):
    """
    Factory method returning a global weather underground instance.

    :param apiKey: The weather underground API key
    :type apiKey: string
    """

    return WeatherUnderground(apiKey, None)

# ------------------------------------------------------------------------------
