#
# Filename     : sun_phase.py
# Description  : 
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from json import dumps

import arrow

# ------------------------------------------------------------------------------

class SunPhase:
    """
    Encapsulate raw sun phase data.
    """

    # --------------------------------------------------------------------------

    def __init__(self,
                 sunriseTime,
                 sunsetTime):
        """
        Constructor

        :param sunriseTime: Timestamp of sunrise
        :type sunriseTime: int
        :param sunsetTime: Timestamp of the sunset
        :type sunsetTime: int
        """

        self._sunriseTime = sunriseTime
        self._sunsetTime = sunsetTime

    # --------------------------------------------------------------------------

    def getSunriseTime(self):
        """
        Return the time of the sunrise.
        """

        return self._sunriseTime

    # --------------------------------------------------------------------------

    def getSunsetTime(self):
        """
        Return the time of the sunset.
        """

        return self._sunsetTime

    # --------------------------------------------------------------------------

    def toJSON(self):
        """
        Return the JSON formatted representation of the object.
        """

        return dumps({'sunsetTime': self._sunriseTime,
                      'sunriseTime': self._sunsetTime})

    # --------------------------------------------------------------------------

    def __str__(self):
        """
        Return the string representation of the object.
        """

        return '{} {}'.format(arrow.get(self._sunriseTime),
                              arrow.get(self._sunsetTime))

# ------------------------------------------------------------------------------
