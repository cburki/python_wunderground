#
# Filename     : exceptions.py
# Description  : Exceptions error classes.
# Author       : Christophe Burki
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from os import linesep

# ------------------------------------------------------------------------------

class APICallError(Exception):
    """
    Error class that represents failures when invoking the WU API.
    """

    # --------------------------------------------------------------------------

    def __init__(self, message, triggeringError=None):
        """
        Constructor

        :param message: The message of the error
        :type message: str
        :param triggeringError: Optional exception object that triggered this error
        :type triggeringError: Exception
        """

        self._message = message
        self._triggeringError = triggeringError

    # --------------------------------------------------------------------------

    def __str__(self):
        """
        Return the string representation of the exception.
        """

        msg = ''.join(['Exception in calling WU web API', linesep,
                       'Reason    : ', self._message])

        if self._triggeringError is not None:
            msg = ''.join([msg, linesep,
                           'Caused by : ', str(self._triggeringError)])

        return msg

# ------------------------------------------------------------------------------

class APIResponseError(Exception):
    """
    Error class that represents failures in the WU API responses.
    """

    # --------------------------------------------------------------------------

    def __init__(self, message, value, triggeringError=None):
        """
        Constructor

        :param message: The message of the error
        :type message: str
        :param triggeringError: Optional exception object that triggered this error
        :type triggeringError: Exception
        """

        self._message = message
        self._value = value
        self._triggeringError = triggeringError

    # --------------------------------------------------------------------------

    def __str__(self):
        """
        Return the string representation of the exception.
        """

        msg = ''.join(['Exception in calling WU web API]', linesep,
                       'Reason    : ', self._message, linesep,
                       'Value     : ', self._value])

        if self._triggeringError is not None:
            msg = ''.join([msg, linesep,
                           'Caused by : ', str(self._triggeringError)])

        return msg

# ------------------------------------------------------------------------------

class ParserError(Exception):
    """
    Error class that represents failures in JSON object parser.
    """

    # --------------------------------------------------------------------------

    def __init__(self, message, value, triggeringError=None):
        """
        Constructor

        :param message: The message of the error
        :type message: str
        :param triggeringError: Optional exception object that triggered this error
        :type triggeringError: Exception
        """

        self._message = message
        self._value = value
        self._triggeringError = triggeringError

    # --------------------------------------------------------------------------

    def __str__(self):
        """
        Return the string representation of the exception.
        """

        msg = ''.join(['Exception in data parsing]', linesep,
                       'Reason    : ', self._message, linesep,
                       'Value     : ', self._value])

        if self._triggeringError is not None:
            msg = ''.join([msg, linesep,
                           'Caused by : ', str(self._triggeringError)])

        return msg

# ------------------------------------------------------------------------------
