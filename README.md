[![build status](https://gitlab.com/cburki/python_wunderground/badges/master/build.svg)](https://gitlab.com/cburki/python_wunderground/commits/master)

---

Summary
-------

A python wrapper around the (Weather Underground API)[https://www.wunderground.com/weather/api/].
It allows quick and easy consumption of weather underground data from python 
application via a simple object model.

More information about this wrapper library and how to use it can be found on the project web site [https://python-wunderground.burkionline.net](https://python-wunderground.burkionline.net).


Dependency
----------

This code depends on the 2 following libraries.

 - [arrow](http://crsmithdev.com/arrow/)
 - [urllib3](https://urllib3.readthedocs.io/en/latest/)

They can be easily installed by issuing this command.

    pip install arrow urllib3


Installation
------------

The simplest way to install the library is using pip.

    pip install git+https://gitlab.burkionline.net:8443/cburki/python_wunderground.git

Or it could be installed from source.

    git clone https://gitlab.com/cburki/python_wunderground.git
    cd python_wunderground
    make install

The make install will take care of installing the dependency.
