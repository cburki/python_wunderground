# 
# Filename     : pywunderground_test.py
# Description  : Testing code.
# Author       : Christophe Burki
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from sys import exit
from os import environ
from unittest import TestCase, main

from pywunderground import WU
from pywunderground.exceptions import APIResponseError

# ------------------------------------------------------------------------------

class Test(TestCase):
    """
    Unit test class.
    """

    # --------------------------------------

    def testBadKey(self):
        """
        Test for a bad API key.
        """

        wu = WU('1234567890')
        self.assertRaises(APIResponseError, wu.weatherAtPlace, 'Switzerland/Gland')

    # --------------------------------------

    def testObservation(self):
        """
        Test observation retrieval.

        The WU_API_KEY environment variables should be given when calling tests. For
        GitLab CI, this variable is configured in the projects settings.
        """

        apiKey = None
        try:
            apiKey = environ['WU_API_KEY']
        except:
            self.fail("Error : Environment variable WU_API_KEY not set")

        try:
            wu = WU(apiKey)
            weather = wu.weatherAtPlace('Switzerland/Gland')

            if weather.getLocation() is None:
                self.fail("Error : No location in observation data")

            if weather.getWeather() is None:
                self.fail("Error : No weather in observation data")

        except Exception as e:
            self.fail("Error : Exception while retrieving observation data : {}".format(str(e)))

    # --------------------------------------

    def testDailyForecast(self):
        """
        Test daily forecast retrieval.
        """

        apiKey = None
        try:
            apiKey = environ['WU_API_KEY']
        except:
            self.fail("Error : Environment variable WU_API_KEY not set")

        try:
            wu = WU(apiKey)
            forecast = wu.dailyForecastAtPlace('Switzerland/Gland')

            if forecast.getInterval() != 'd':
                self.fail("Error : Not a daily forecast")

            if forecast.getLocation() is None:
                self.fail("Error : No location in daily forecast data")

            if len(forecast.getWeathers()) == 0:
                self.fail("Error : No weather in daily forecast data")

        except Exception as e:
            self.fail("Error : Exception while retrieving daily forecast data : {}".format(str(e)))

    # --------------------------------------

    def testHourlyForecast(self):
        """
        Test hourly forecast retrieval.
        """

        apiKey = None
        try:
            apiKey = environ['WU_API_KEY']
        except:
            self.fail("Error : Environment variable WU_API_KEY not set")

        try:
            wu = WU(apiKey)
            forecast = wu.hourlyForecastAtPlace('Switzerland/Gland')

            if forecast.getInterval() != 'h':
                self.fail("Error : Not a hourly forecast")

            if forecast.getLocation() is None:
                self.fail("Error : No location in hourly forecast data")

            if len(forecast.getWeathers()) == 0:
                self.fail("Error : No weather in hourly forecast data")

        except Exception as e:
            self.fail("Error : Exception while retrieving hourly forecast data : {}".format(str(e)))

    # --------------------------------------

    def testAstronomy(self):
        """
        Test astronomy retrieval.
        """

        apiKey = None
        try:
            apiKey = environ['WU_API_KEY']
        except:
            self.fail("Error : Environment variable WU_API_KEY not set")

        try:
            wu = WU(apiKey)
            wu.astronomyAtPlace('Switzerland/Gland')

        except Exception as e:
            self.fail("Error : Exception while retrieving astronomy data : {}".format(str(e)))

# ------------------------------------------------------------------------------

if (__name__ == "__main__"):

    main()
    print("OK")
    exit(0)

# ------------------------------------------------------------------------------
